package com.orlandosantana.primeraapp4c;

import android.app.Activity;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.*;
import android.widget.Button;


public class Fragmentos extends Activity implements View.OnClickListener,  FrgUno.OnFragmentInteractionListener, FrgDos.OnFragmentInteractionListener {


    Button Fragmentouno, Fragmentodos;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fragmentos);
        Fragmentouno= (Button) findViewById(R.id.btnuno);
        Fragmentodos= (Button) findViewById(R.id.btndos);

        Fragmentouno.setOnClickListener(this);
        Fragmentodos.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnuno:
                FrgUno fragmentoUno = new FrgUno();
                //getActivity().getSupportFragmentManager().beginTransaction();
                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                transaction.replace(R.id.contenedor,fragmentoUno);
                transaction.commit();
                break;
        }
        switch (v.getId()){
            case R.id.btndos:
                FrgDos fragmentoDos = new FrgDos();
                //getActivity().getSupportFragmentManager().beginTransaction();
                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                transaction.replace(R.id.contenedor,fragmentoDos);
                transaction.commit();
                break;

        }
    }
    @Override
    public void onFragmentInteraction(Uri uri) {

    }




    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent;

        switch (item.getItemId()){
            case R.id.opcionLogin:
                intent = new Intent(Fragmentos.this, login.class);
                startActivity(intent);
                break;
        }
        switch (item.getItemId()){
            case R.id.opcionRegistrar:
                intent = new Intent(Fragmentos.this, registrar.class);
                startActivity(intent);
                break;
        }
        switch (item.getItemId()){
            case R.id.opcionVibracion:
                intent = new Intent(Fragmentos.this, ActividadVibrar.class);
                startActivity(intent);
                break;
        }
        switch (item.getItemId()){
            case R.id.opcionAcelerometro:
                intent = new Intent(Fragmentos.this, SensorAcelerometro.class);
                startActivity(intent);
                break;
        }
        switch (item.getItemId()){
            case R.id.opcionCamara:
                intent = new Intent(Fragmentos.this, Camara.class);
                startActivity(intent);
                break;
        }
        switch (item.getItemId()){
            case R.id.opcionLinterna:
                intent = new Intent(Fragmentos.this, Linterna.class);
                startActivity(intent);
                break;
        }
        switch (item.getItemId()){
            case R.id.opcionProximidad:
                intent = new Intent(Fragmentos.this, SensorProximidad.class);
                startActivity(intent);
                break;
        }
        switch (item.getItemId()){
            case R.id.opcionBrujula:
                intent = new Intent(Fragmentos.this, SensorBrujula.class);
                startActivity(intent);
                break;
        }

        return true;
    }

  }


