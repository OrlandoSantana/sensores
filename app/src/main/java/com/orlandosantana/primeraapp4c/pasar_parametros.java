package com.orlandosantana.primeraapp4c;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class pasar_parametros extends Activity {
    EditText cajaDatos;
    Button botonEnviar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pasar_parametros);

        cajaDatos = (EditText) findViewById(R.id.txtenviar);
        botonEnviar = (Button) findViewById(R.id.btnenviar);

        botonEnviar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(pasar_parametros.this, recibir_parametros.class);
                Bundle bundle = new Bundle();
                bundle.putString("dato", cajaDatos.getText().toString());
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });
    }
}
