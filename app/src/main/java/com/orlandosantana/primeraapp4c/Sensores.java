package com.orlandosantana.primeraapp4c;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class Sensores extends Activity {
Button btnProximidad, btnacelerometro, btnbrujula;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sensores);
        btnProximidad =  (Button) findViewById(R.id.btnproximidad);
        btnacelerometro=  (Button) findViewById(R.id.btnacelerometro);
        btnbrujula=  (Button) findViewById(R.id.btnbrujula);

        btnacelerometro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Sensores.this, SensorAcelerometro.class);
                startActivity(intent);
            }
        });
        btnProximidad.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent  intent = new Intent(Sensores.this, SensorProximidad.class);
                startActivity(intent);
            }
        });
        btnbrujula.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent  intent = new Intent(Sensores.this, SensorBrujula.class);
                startActivity(intent);
            }
        });
    }
}
