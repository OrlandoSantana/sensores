package com.orlandosantana.primeraapp4c;

import android.app.Activity;
import android.hardware.Camera;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;

public class Linterna extends Activity {
    ImageButton buttonLinterna;
    Camera camera;
    boolean turnon;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_linterna);

        buttonLinterna = (ImageButton) findViewById(R.id.imageButton);

        buttonLinterna.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {
                    camera= Camera.open();
                    Camera.Parameters parameters = camera.getParameters();
                    parameters.setFlashMode(Camera.Parameters.FLASH_MODE_TORCH);
                    camera.setParameters(parameters);
                    camera.startPreview();
                }catch (Exception e){
                    camera.stopPreview();
                    camera.release();
                }
            }
        });

    }
}
